const { MokkAPI } = require('./index.js')

const mokkAPI = new MokkAPI(process.argv[2])
mokkAPI.uploadConfig(process.argv.slice(3)).catch((err) => {
  console.error('Error: ', err.message)
  process.exit(1)
})
