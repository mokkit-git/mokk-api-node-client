const superagent = require('superagent')
const glob = require('glob')
const fs = require('fs')


class RequestError extends Error {
  constructor(type, status, message) {
    super();
    this.type = type
    this.status = status
    this.message = message;
  }
}

function handleError(error) {
  let type;
  if (400 <= error.status <= 499) {
    type = 'parameterError'
  } else if (500 <= error.status <= 599) {
    type = 'serverError'
  } else {
    type = 'unknownError'
  }
  throw new RequestError(type, error.status, error.response.text)
}

exports.MokkAPI = class {
  constructor(url) {
    this.url = url + (url.charAt(url.length - 1) === '/' ? '' : '/')  // append '/' if not already there
  }

  async putDefinedMockRule(rule_id, path, methods, persistence,
                           response_status, response_content_type, response_content, response_headers) {
    try {
      const response = await superagent('PUT', this.url + 'mokk/rules/' + rule_id)
        .send({
          path: path,
          methods: methods,
          persistence: persistence,
          response_definition: {
            status: response_status,
            content_type: response_content_type,
            content: response_content,
            headers: response_headers
          },
        })
      return response.body
    } catch (error) {
      handleError(error)
    }
  }

  async getMatchCount(rule_id) {
    try {
      const response = await superagent('GET', this.url + 'mokk/rules/' + rule_id).send()
      return parseInt(response.body['match_count'])
    } catch (error) {
      handleError(error)
    }
  }

  async uploadConfig(paths) {
    if (paths.length === 0) {
      throw new Error('Please specify at least one path to a config file.')
    }

    for (const path of paths) {
      console.log('Uploading ' + path)
      let file = fs.readFileSync(path)
      await superagent('POST', this.url + 'mokk/upload_rule_file')
        .set('Content-Type', 'application/yaml')
        .send(file)
        .catch((err) => {
          if (err.response) {
            throw new Error(err.response.text)
          }
          throw err
        })
    }
    console.log('Upload complete')
  }

}
